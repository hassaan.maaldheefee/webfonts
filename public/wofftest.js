/*
  WebFont Demo JavaScript
  
  (C) 2013-2019 High-Logic B.V.
  
  http://www.high-logic.com
*/

var haveErrors = false;
var haveOTLF   = false;

function Initialize() {
  if (symbolfont) {
    AddError("Symbol fonts are legacy Windows fonts. They may not be supported by other operating systems, and word wrapping and spell check features won't work. Unicode fonts can contain symbols, so we recommend to convert your Symbol font to a Unicode font, unless you need to support old software. In FontCreator go to the main menu and select Tools -> Convert Font -> Convert to Unicode Font.");
  }
  BrowserCheck(otf.length > 0, colorfont);

  InitializeOpenTypeFeatures();
  
  vElement = document.getElementById("input");
  if (vElement == null) {
    AddError("Failed to load font.");  
  } else {
    UpdatePreviewText(vElement);
  }
  
  ProcessErrors();
}

function AddError(aError) {
  haveErrors = true;
  id = document.getElementById('errorlist');
  id.innerHTML += "<li>" + aError + "</li>";
  return false;
}

function ProcessErrors() {
  if (!haveErrors) {
    document.getElementById('errors').style.display = "none";
  }
  else {
    document.getElementById("nojserror").style.display = "none";
  }
}

function DetermineBrowser() {
  var browserstr = navigator.userAgent.toLowerCase();
  var result = Array();
  result["name"] = "unknown";
  result["fullname"] = "unknown";
  result["version"] = "unknown";
  if (match = browserstr.match("edge/([0-9]+)")) {
    result["name"] = "edge";
    result["fullname"] = "Microsoft Edge";
    result["version"] = match[1];
  }
  else if (match = browserstr.match("chrome/([0-9]+)")) {
    result["name"] = "chrome";
    result["fullname"] = "Google Chrome";
    result["version"] = match[1];
  }
  else if (match = browserstr.match("firefox/([0-9]+)")) {
    result["name"] = "firefox";
    result["fullname"] = "Mozilla Firefox";
    result["version"] = match[1];
  }
  else if (match = browserstr.match("opera/([0-9]+)")) {
    result["name"] = "opera";
    result["fullname"] = "Opera";
    result["version"] = match[1];
  }    
  else if (match = browserstr.match("safari/([0-9]+)")) {
    result["name"] = "safari";
    result["fullname"] = "Apple Safari";
    if (match = browserstr.match("version/([0-9\.]+)")) {
      result["version"] = match[1];
    }
  }
  else if (match = browserstr.match("msie ([0-9]+)")) {
    result["name"] = "ie";
    result["fullname"] = "Microsoft Internet Explorer";
    result["version"] = match[1];
  }  
  else if (match = browserstr.match("trident/([0-9]+).+; .*? rv:([0-9]+)")) {
    /* IE 11 and up */
    if (match[1] >= 7) {
      result["name"] = "ie";
      result["fullname"] = "Microsoft Internet Explorer";
      result["version"] = match[2];
    }
  }   
  return result;
}

function DetermineOS() {
  var browserstr = navigator.userAgent.toLowerCase();
  var result = Array();
  result["name"] = "unknown";
  result["version"] = "unknown";
  if (match = browserstr.match("windows.*? ([0-9\.]+);")) {
    result["name"] = "windows";
	result["version"] = match[1];
  }
  if (browserstr.indexOf("mac os x") > -1) {
    if (browserstr.indexOf("mobile") > -1) {
      result["name"] = "ios";
    } else {
      result["name"] = "osx";
    }
  }
  return result;
}

function html2text(html) {
var temp = document.createElement("div");
   temp.innerHTML = html;
   return temp.textContent || temp.innerText || "";
}

function BrowserCheck(aOTLF, aColor) {
 
  var browser = DetermineBrowser();
  var os = DetermineOS();
  var id = document.getElementById("errorlist");
  
  switch (browser["name"]) {
    case "firefox" :
      if (aOTLF) {
	      if (browser["version"] < 4) {
            return AddError("You need at least FireFox 4 or a CSS 3.0 compatible browser in order to use OpenType Layout Features.");
          }
  	  }
	    if (aColor) {
	      if (browser["version"] < 32) {
	        return AddError("You need at least Firefox 32 in order to use color fonts.");
        }
	    }
    break;
    case "chrome" : 
      if (browser["version"] < 22) {
        return AddError("You need at least Chrome 22 or a CSS 3.0 compatible browser in order to use OpenType Layout Features.");
      }
  	  if (aColor) {
	      if (browser["version"] < 40) {
	        return AddError("You need at least Chrome 40 in order to use color fonts.");
        }
	    }
    break;
    case "edge" :
      // Should all work... 
    break;
    case "ie" : 
      if (aOTLF && (browser["version"] < 10)) {
        return AddError("You need at least Internet Explorer 10 or a CSS 3.0 compatible browser in order to use OpenType Layout Features.");
      }
	    if (aColor) {
		    if (browser["version"] < 11) {
	        return AddError("You need at least Internet Explorer 11 in order to use color fonts.");
		    }
		    else {
		      if (os["version"] < 6.3) {
		        return AddError("You need at least Windows 8.1 to use color fonts with Internet Explorer. Alternatively you can use Firefox which supports color fonts since version 32 on all platforms.");
		      }
		    }
	    }
    break;
    case "opera" : 
      if (browser["version"] < 15) {
        return AddError("You need at least Opera 15 or a CSS 3.0 compatible browser in order to use OpenType Layout Features.");
      }
      else {
        return AddWarning("Warning: This version of Opera might not support OpenType Layout Features.");
      }
	    if (aColor) {
	      return AddError("Opera does not support color fonts at least up to version 16.");
	    }
    break;
    case "safari" : 
      if (os["name"] = "osx" ) {
        if (browser["version"] < 9.1) {
          return AddError("You need at least Safari 9.1 or a CSS 3.0 compatible browser in order to use OpenType Layout Features.");
        }
    	  if (aColor) {
	        return AddError("Safari does not support color fonts at least up to version 9.1.");
	      }
      }
      if (os["name"] = "ios" ) {
        if (browser["version"] < 9.3) {
          return AddError("You need at least Mobile Safari 9.3 or a CSS 3.0 compatible browser in order to use OpenType Layout Features.");
        }
    	  if (aColor) {
	        return AddError("Mobile Safari does not support color fonts at least up to version 9.3.");
	      }
      }
    break;
    default :
      return AddError("Unable to determine your browser and/or version. OpenType Layout Features might not display correctly.");
    break;
  }
}

function UpdateFeatures() {
  var featurestring = "";

  altval = document.getElementById("alternate").value;
  
  for (i = 0; i < otf.length; i++) {
    id = document.getElementById("otf_" + otf[i][0]);
    if (i > 0) {
      featurestring += ",";
    }
    featurestring += "'" + otf[i][0] + "' " + (id.checked ? altval : "0");
  }  
  id = document.getElementById("preview");
  // Firefox 4+
  id.style.MozFontFeatureSettings = featurestring;
  // Chrome 20+ / Safari (OSX only)
  id.style.webkitFontFeatureSettings = featurestring;
  // Official CSS 3.0
  id.style.fontFeatureSettings = featurestring;
}

function SetAllFeatures(aValue) {
  for (i = 0; i < otf.length; i++) {
    id = document.getElementById("otf_" + otf[i][0]);
    id.checked = aValue;
  }  
  UpdateFeatures();
}

function SetDefaultFeatures() {
  for (i = 0; i < otf.length; i++) {
    id = document.getElementById("otf_" + otf[i][0]);
    id.checked = otf[i][2];
  }  
  UpdateFeatures();
}

function InitializeOpenTypeFeatures() {
  id = document.getElementById("otfeatures");
  if (otf.length == 0) {
    id.innerHTML += "This font does not contain any OpenType Layout Features.";
    return;
  
  }
  id.innerHTML += "<input type='button' value='Disable all' onclick='SetAllFeatures(false)'/> ";
  id.innerHTML += "<input type='button' value='Enable all' onclick='SetAllFeatures(true)'/> ";
  id.innerHTML += "<input type='button' value='Default' onclick='SetDefaultFeatures()'/><hr/>";
  for (i = 0; i < otf.length; i++) {
    checked = otf[i][2] ? " checked='checked'" : "";
    id.innerHTML += "<label><input id='otf_"+otf[i][0]+"'"+checked+" type='checkbox' onclick='UpdateFeatures()'> " + otf[i][0] + " - " + otf[i][1] + "</label><br/>";
  }
  UpdateFeatures();
}

function UpdatePreviewText(sender) {
  id = document.getElementById("preview");
  id.innerHTML = sender.value.replace(/\n/g, "<br/>");
}

function UpdatePreviewQuickPick(sender) {
  id = document.getElementById("input");
  switch (sender.value) {
    case "0" : 
      id.value = id.defaultValue;
    break;
    case "1" : 
      id.value = "abcdefghijklmnopqrstuvwxyz\r\nABCDEFGHIJKLMNOPQRSTUVWXYZ\r\n1234567890";
    break;
    case "2" : 
      id.value = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed molestie augue vel quam dapibus sollicitudin. Sed vestibulum erat ac nibh varius vestibulum. Quisque a quam at est sodales pulvinar ac vel nisl. Cras sit amet consectetur dolor. Aliquam nisl lorem, iaculis eget aliquam pharetra, tincidunt id turpis. Ut sit amet nisl sed ipsum tempor luctus. Aenean vitae tristique sem. Pellentesque turpis dolor, lacinia id fermentum et, dapibus ac ipsum. Etiam nisi quam, feugiat vitae eleifend vel, luctus et mi.";
    break;
  }
  UpdatePreviewText(document.getElementById("input"));
}

function UpdateFontSize(sender) {
  id = document.getElementById("preview");
  id.style.fontSize = sender.value + "pt";
}

function UpdateFontColorBG(sender) {
  id = document.getElementById("preview");
  id.style.background = "#" + sender.value;
}

function UpdateFontColorFG(sender) {
  id = document.getElementById("preview");
  id.style.color = "#" + sender.value;
}

function UpdateLanguage(sender) {
  id = document.getElementById("preview");
  id.lang = sender.value;
}

function UpdateAlternate(sender) {
  UpdateFeatures();
}

function UpdateDirection(sender) {
  id = document.getElementById("preview");
  id.style.direction = sender.value;
}